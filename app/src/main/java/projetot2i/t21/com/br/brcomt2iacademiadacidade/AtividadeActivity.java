package projetot2i.t21.com.br.brcomt2iacademiadacidade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.AcademiaDaCidade;

public class AtividadeActivity extends AppCompatActivity {

    private TextView mTextNameAtv;
    private TextView mTextBairroAtv;
    private TextView mCnesAtv;
    private TextView mFoneAtv;
    private TextView mModalidadeAtv;

    private AcademiaDaCidade mAcademia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atividade);
        String modalidade;

        mAcademia = (AcademiaDaCidade) getIntent().getSerializableExtra(DetalheActivity.EXTRA_ATIVIDADE);

        mTextNameAtv = (TextView) findViewById(R.id.txtNomeAtividade);
        mTextBairroAtv = (TextView) findViewById(R.id.txtBarroAtividade);
        mCnesAtv = (TextView) findViewById(R.id.txtCnesAtividade);
        mFoneAtv = (TextView) findViewById(R.id.txtFoneAtividade);
        mModalidadeAtv = (TextView) findViewById(R.id.txtFoneAtividade);

        if (mAcademia != null){
            mTextNameAtv.setText(mAcademia.getAcademiaNome());
            mTextBairroAtv.setText(mAcademia.getBairro());
            mCnesAtv.setText(mAcademia.getCnes().toString());
            mFoneAtv.setText(mAcademia.getFone());

            modalidade = mAcademia.getModalidades();
            modalidade = modalidade.replace(" ", "");
            modalidade = modalidade.replaceAll(",", "\n");

            mModalidadeAtv.setText(modalidade);

        }

    }
}
