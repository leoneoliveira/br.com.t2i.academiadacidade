package projetot2i.t21.com.br.brcomt2iacademiadacidade.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Leone Oliveira on 08/01/2018.
 */

public class Recife implements Serializable {
    @SerializedName("AcademiaDaCidade")
    private List<AcademiaDaCidade> academiaDaCidade;

    public List<AcademiaDaCidade> getAcademiaDaCidade() {
        return academiaDaCidade;
    }

    public void setAcademiaDaCidade(List<AcademiaDaCidade> academiaDaCidade) {
        this.academiaDaCidade = academiaDaCidade;
    }
}

