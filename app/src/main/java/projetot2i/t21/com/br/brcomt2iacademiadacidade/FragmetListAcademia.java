package projetot2i.t21.com.br.brcomt2iacademiadacidade;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.AcademiaDaCidade;
import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.Recife;
import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.Root;

/**
 * Created by Leone Oliveira on 06/01/2018.
 */

public class FragmetListAcademia extends Fragment {

    private static FragmetListAcademia myFragment;
    private ListView itensListView;
    private List<AcademiaDaCidade> mAcademia;
    private ProgressDialog mProgress;
    private AcademiaAdpt mAcademiaAdapter;

    public static final String EXTRA_DETALHE = "detalhe";

    public static FragmetListAcademia newInstance() {
        if (myFragment == null) {
            myFragment = new FragmetListAcademia();
        }
        return myFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragmet_list, container, false);

        if (Util.getInstance(getContext()).isOnline()) {
            itensListView = root.findViewById(R.id.lvTeams);
            mAcademia = new ArrayList<>();
            new AcademiaTask().execute();
            Util.getInstance(getContext()).cancelNotification();
        }else {
            Util.getInstance(getContext()).createNotification();
            Util.getInstance(getContext()).notifyListener();
            Toast.makeText(getContext(), R.string.ativar_internet, Toast.LENGTH_LONG).show();
        }
        return root;
    }

    private void setupListView() {
        mAcademiaAdapter = new AcademiaAdpt(getActivity(), mAcademia);

        itensListView.setAdapter(mAcademiaAdapter);
        itensListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AcademiaDaCidade academia = (AcademiaDaCidade) adapterView.getItemAtPosition(i);
                Intent it = new Intent(getActivity(), DetalheActivity.class);
                it.putExtra(EXTRA_DETALHE, academia);
                Log.i("leone", EXTRA_DETALHE);
                startActivity(it);

            }
        });
    }

    class AcademiaTask extends AsyncTask<Void, Void, Recife> {


        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(getActivity());
            mProgress.setTitle("Aguarde");
            //  mProgress.setIcon(getDrawable(R.drawable.icon));
            mProgress.setMessage("Carregando ....");
            mProgress.show();
        }


        @Override
        protected Recife doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url("https://dl.dropboxusercontent.com/s/0wxiggbvifq2ax8/academia.json")
                    .build();
            Response response = null;

            try {
                response = client.newCall(request).execute();
                String s = response.body().string();
                Log.i("TEST", s);
                Gson gson = new Gson();
                Root recife = gson.fromJson(s, Root.class);
                List<AcademiaDaCidade> timesTemp = recife.getRecife().getAcademiaDaCidade();
                mAcademia.addAll(timesTemp);
            } catch (Throwable e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Recife recife) {

            if (mProgress != null && mProgress.isShowing()) {
                try {
                    mProgress.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            setupListView();
        }

    }
}