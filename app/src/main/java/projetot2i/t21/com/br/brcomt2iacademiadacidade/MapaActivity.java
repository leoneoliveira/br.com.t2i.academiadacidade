package projetot2i.t21.com.br.brcomt2iacademiadacidade;

import android.Manifest;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.AcademiaDaCidade;


public class MapaActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_MAP = 1;

    private AcademiaDaCidade mAcademia;
    private TextView mTextName;
    private TextView mTextDetail;
    private Button mBtnVerLocAppExterno;
    private GoogleMap mMap;
    private InternalBroadcastAbrirRota mBroadcastInterno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        mAcademia = (AcademiaDaCidade) getIntent().getSerializableExtra(DetalheActivity.EXTRA_ACADEMIA);

        mTextName = (TextView) findViewById(R.id.txt_place_name);
        mTextDetail = (TextView) findViewById(R.id.txt_place_details);
        mBtnVerLocAppExterno = findViewById(R.id.btn_abrir_loc_app_externo);
        mBroadcastInterno = new InternalBroadcastAbrirRota();

        if (mAcademia != null) {
            mTextName.setText(mAcademia.getAcademiaNome());
            mTextDetail.setText(mAcademia.getEndereco());
        }

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mBroadcastInterno, new IntentFilter(InternalBroadcastAbrirRota.INTERNAL_BROADCAST_ACTION));

        mBtnVerLocAppExterno.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LocalBroadcastManager localBM = LocalBroadcastManager.getInstance(MapaActivity.this);
                Intent it = new Intent(InternalBroadcastAbrirRota.INTERNAL_BROADCAST_ACTION);
                it.putExtra(InternalBroadcastAbrirRota.INTERNAL_BROADCAST_EXTRA, mAcademia);
                localBM.sendBroadcast(it);
            }
        });

        checkPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Unregister the local broadcats when activity is going to foreground
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mBroadcastInterno);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Register receiver when activity is being created or returning from foreground
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mBroadcastInterno,  new IntentFilter(InternalBroadcastAbrirRota.INTERNAL_BROADCAST_ACTION));
    }

    private void setupMap() {
        MapFragment fragment = ((MapFragment)getFragmentManager().findFragmentById(R.id.mapview));

        fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mMap.setMyLocationEnabled(true);

                double latitude = mAcademia.getLatitude();
                double longitude = mAcademia.getLongitude();
                String title = mAcademia.getAcademiaNome();

                LatLng placeCoords = new LatLng(latitude, longitude);

                mMap.addMarker(new MarkerOptions().position(placeCoords)
                        .title(title));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placeCoords,15));
            }
        });
    }

    private void checkPermission() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            setupMap();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_MAP);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_MAP: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupMap();
                } else {

                }
                return;
            }
        }
    }
}
