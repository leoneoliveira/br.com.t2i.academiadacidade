package projetot2i.t21.com.br.brcomt2iacademiadacidade.Model;

import java.io.Serializable;

/**
 * Created by Leone Oliveira on 06/01/2018.
 */

public class AcademiaDaCidade implements Serializable {

    private Integer rpa;
    private Integer microRegiao;
    private Integer cnes;
    private String academiaNome;
    private String endereco;
    private String bairro;
    private String fone;
    private Double latitude;
    private Double longitude;
    private String horarioDeAulas;
    private String img;
    private String modalidades;

    public Integer getRpa() {
        return rpa;
    }

    public void setRpa(Integer rpa) {
        this.rpa = rpa;
    }

    public Integer getMicroRegiao() {
        return microRegiao;
    }

    public void setMicroRegiao(Integer microRegiao) {
        this.microRegiao = microRegiao;
    }

    public Integer getCnes() {
        return cnes;
    }

    public void setCnes(Integer cnes) {
        this.cnes = cnes;
    }

    public String getAcademiaNome() {
        return academiaNome;
    }

    public void setAcademiaNome(String academiaNome) {
        this.academiaNome = academiaNome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getHorarioDeAulas() {
        return horarioDeAulas;
    }

    public void setHorarioDeAulas(String horarioDeAulas) {
        this.horarioDeAulas = horarioDeAulas;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getModalidades() {
        return modalidades;
    }

    public void setModalidades(String modalidades) {
        this.modalidades = modalidades;
    }

}

