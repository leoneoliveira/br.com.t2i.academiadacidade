package projetot2i.t21.com.br.brcomt2iacademiadacidade.Model;

import java.io.Serializable;

/**
 * Created by Leone Oliveira on 08/01/2018.
 */

public class Root implements Serializable {

    private Recife recife;

    public Recife getRecife() {
        return recife;
    }

    public void setRecife(Recife recife) {
        this.recife = recife;
    }
}
