package projetot2i.t21.com.br.brcomt2iacademiadacidade;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.AcademiaDaCidade;

public class DetalheActivity extends AppCompatActivity {

    private ImageView mImgDetalhe;
    private TextView mTextName;
    private TextView mTextBairro;
    private TextView mHorario;
    private Button mAbrirLocalizacao;
    private Button mAbrirAtividade;

    private AcademiaDaCidade mAcademia;

    public static final String EXTRA_ACADEMIA = "projetot2i.t21.com.br.brcomt2iacademiadacidade.extra_academia";
    public static final String EXTRA_ATIVIDADE = "projetot2i.t21.com.br.brcomt2iacademiadacidade.extra_atividade";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        mAcademia = (AcademiaDaCidade) getIntent().getSerializableExtra(FragmetListAcademia.EXTRA_DETALHE);

        mImgDetalhe = (ImageView) findViewById(R.id.imgDetalhe);
        mTextName = (TextView) findViewById(R.id.txtNomeDetalhe);
        mTextBairro = (TextView) findViewById(R.id.txtBarroDetalhe);
        mHorario = (TextView) findViewById(R.id.txtHorioDetalhe);
        mAbrirAtividade = findViewById(R.id.btn_ver_atividade);
        mAbrirLocalizacao = findViewById(R.id.btn_ver_localizacao);

        if (mAcademia != null) {
            Picasso.with(DetalheActivity.this)
                    .load(mAcademia.getImg())
                    .into(mImgDetalhe);
            mTextName.setText(mAcademia.getAcademiaNome());
            mTextBairro.setText(mAcademia.getBairro());
            mHorario.setText(mAcademia.getHorarioDeAulas());

        }


        mAbrirLocalizacao.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent it = new Intent(DetalheActivity.this, MapaActivity.class);
                it.putExtra(EXTRA_ACADEMIA, mAcademia);
                startActivity(it);
            }
        });


        mAbrirAtividade.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent it = new Intent(DetalheActivity.this, AtividadeActivity.class);
                it.putExtra(EXTRA_ATIVIDADE, mAcademia);
                startActivity(it);
            }
        });

    }
}
