package projetot2i.t21.com.br.brcomt2iacademiadacidade;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.AcademiaDaCidade;

/**
 * Created by Ivan Cantalice on 10/01/2018.
 */

public class InternalBroadcastAbrirRota extends BroadcastReceiver {
    public static final String INTERNAL_BROADCAST_ACTION = "br.com.dts.broadcastreceiver.INTERNAL_BROADCAST_ABRIR_ROTA_ACTION";
    public static final String INTERNAL_BROADCAST_EXTRA = "br.com.dts.broadcastreceiver.INTERNAL_BROADCAST_ABRIR_ROTA_EXTRA";

    private AcademiaDaCidade mAcademia;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(INTERNAL_BROADCAST_ACTION)) {
            Toast.makeText(context, R.string.abrir_rota, Toast.LENGTH_LONG).show();

            mAcademia = (AcademiaDaCidade) intent.getSerializableExtra(INTERNAL_BROADCAST_EXTRA);
            Log.d("ivan", "onReceive: " + mAcademia.toString());
            if (mAcademia != null) {
                //Uri gmmIntentUri = Uri.parse("geo:" + mAcademia.getLatitude() + "," + mAcademia.getLongitude());
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mAcademia.getLatitude() + ", " + mAcademia.getLongitude());

                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(mapIntent);
                }
            }
        }
    }
}
