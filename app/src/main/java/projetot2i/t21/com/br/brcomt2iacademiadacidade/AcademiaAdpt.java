package projetot2i.t21.com.br.brcomt2iacademiadacidade;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import projetot2i.t21.com.br.brcomt2iacademiadacidade.Model.AcademiaDaCidade;

/**
 * Created by Leone Oliveira on 06/01/2018.
 */

public class AcademiaAdpt extends BaseAdapter {

    private Context context;
    private List<AcademiaDaCidade> academiaCidade;

    public AcademiaAdpt(Context context, List<AcademiaDaCidade> academiaCidade) {
        this.context = context;
        this.academiaCidade = academiaCidade;
    }

    @Override
    public int getCount() {
        return (academiaCidade != null) ? academiaCidade.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return academiaCidade.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        View itemView;
        AcademiaDaCidade academiaDaCidade = academiaCidade.get(i);

        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            itemView = inflater.inflate(R.layout.list_academia, viewGroup, false);

            holder.imgAcademia = itemView.findViewById(R.id.imLogo);
            holder.txtNome = itemView.findViewById(R.id.txtNome);
            holder.txtBairro = itemView.findViewById(R.id.txtBairro);

            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
            itemView = view;
        }

        Picasso.with(context)
                .load(academiaDaCidade.getImg())
                .into(holder.imgAcademia);

        holder.txtNome.setText(academiaDaCidade.getAcademiaNome());
        holder.txtBairro.setText(academiaDaCidade.getBairro());

        return itemView;
    }

    private static class ViewHolder {
        ImageView imgAcademia;
        TextView txtNome;
        TextView txtBairro;
    }
}
